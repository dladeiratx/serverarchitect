package ladeira.general;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import ladeira.attributes.AttributesHandler;
import ladeira.attributes.PermissionHandler;
import ladeira.events.ChatEvent;
import ladeira.events.CommandProcessEvent;
import ladeira.events.InventorySelectEvent;
import ladeira.events.JoinLeaveEvent;
import ladeira.events.PlayerDieEvent;
import ladeira.events.PlayerDropEvent;
import ladeira.events.PlayerUseEvent;
import ladeira.vanish.VanishHandler;

public class ServerHandler extends JavaPlugin {
	
	private static Plugin plugin;
	
	private static FileConfiguration attributesConfig;
	private static FileConfiguration messagesConfig;
	private static FileConfiguration playersConfig;
	private static FileConfiguration scoreboardsConfig;
	private static FileConfiguration infoConfig;
	
	private static File attributesFile;
	private static File messagesFile;
	private static File playersFile;
	private static File scoreboardsFile;
	private static File infoFile;
	
	@Override
	public void onEnable() {
		plugin = this; // Should be before everyone else
		loadFileVariables(); // Before loadConfigs
		loadConfigs(); // After loadFileVariables
		loadConfigVariables(); // After loadConfigs
		loadConfigDefaults(); // After everyting
		
		// Set variables for classes
		AttributesHandler.loadAttributes();
		VanishHandler.loadVanishMessages();
		
		// Events, boring but needed
		registerEvents(this, new JoinLeaveEvent(), new ChatEvent(), new PlayerDieEvent(), new CommandProcessEvent(), new PlayerUseEvent(), new InventorySelectEvent(), new PlayerDropEvent());
		
		@SuppressWarnings("unused")
		Metrics metrics = new Metrics(this); // bStats
	}
	
	public void onDisable() {
		plugin = null; // Should be after everything else
	}
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
	
	public static Plugin getPlugin() { return plugin; }
	
	public void loadConfigs() {
		try {
			// attributesConfig
			attributesFile.getParentFile().mkdirs();
			if (attributesFile.createNewFile()) {
				getLogger().info("Created file: " + attributesFile.getName());
			} else {
				getLogger().info("Config file exists: " + attributesFile.getName());
			}
			
			// messagesConfig
			messagesFile.getParentFile().mkdirs();
			if (messagesFile.createNewFile()) {
				getLogger().info("Created file: " + messagesFile.getName());
			} else {
				getLogger().info("Config file exists: " + messagesFile.getName());
			}
			
			// playersConfig
			playersFile.getParentFile().mkdirs();
			if (playersFile.createNewFile()) {
				getLogger().info("Created file: " + playersFile.getName());
			} else {
				getLogger().info("Config file exists: " + playersFile.getName());
			}
			
			// scoreboardsConfig
			scoreboardsFile.getParentFile().mkdirs();
			if (scoreboardsFile.createNewFile()) {
				
				getLogger().info("Created file: " + scoreboardsFile.getName());
			} else {
				getLogger().info("Config file exists: " + scoreboardsFile.getName());
			}
			// scoreboardsConfig
			infoFile.getParentFile().mkdirs();
			if (infoFile.createNewFile()) {
				getLogger().info("Created file: " + infoFile.getName());
			} else {
				getLogger().info("Config file exists: " + infoFile.getName());
			}
		} catch (IOException e) {
			getLogger().severe("Got IOException when creating/loading configuration files");
		}
	}
	
	public void loadConfigVariables() {
		ServerHandler.attributesConfig = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "attributes.yml"));
		ServerHandler.messagesConfig = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "messages.yml"));
		ServerHandler.playersConfig = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "players.yml"));
		ServerHandler.scoreboardsConfig = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "scorescoreboards.yml"));
		ServerHandler.infoConfig = YamlConfiguration.loadConfiguration(new File(this.getDataFolder() + "/bin", "info.yml"));
		
	}
	
	public void loadFileVariables() {
		attributesFile = new File(this.getDataFolder(), "attributes.yml");
		messagesFile = new File(this.getDataFolder(), "messages.yml");
		playersFile = new File(this.getDataFolder(), "players.yml");
		scoreboardsFile = new File(this.getDataFolder(), "scorescoreboards.yml");
		infoFile = new File(this.getDataFolder() + "/bin", "info.yml");
	}

	public void loadConfigDefaults(){
		FileConfiguration msgConfig = ServerHandler.getMSGConfig();
		FileConfiguration attrConfig = ServerHandler.getAttrConfig();
		
		//JoinLeave
		msgConfig.addDefault("leave", "[player] &7left joined the server");
		msgConfig.addDefault("join", "[player] &7joined joined the server");
		
		// Deaths
		msgConfig.addDefault("death.tnt", "[player]&7 has been obliterated by tnt");
		msgConfig.addDefault("death.cactus", "[player]&7 has fallen asleep next to a cactus");
		msgConfig.addDefault("death.cramming", "[player]&7 has been pushed by a entity too hard");
		msgConfig.addDefault("death.dragon", "[player]&7 has died to a dragon");
		msgConfig.addDefault("death.drowning", "[player]&7 has breathed in the wrong element");
		msgConfig.addDefault("death.Default", "[player]&7 has died to death");
		msgConfig.addDefault("death.mob", "[player]&7 has been KO\'d by a mob");
		msgConfig.addDefault("death.creeper", "[player]&7 pressed the wrong button on his vest");
		msgConfig.addDefault("death.fall", "[player]&7 has hugged the ground too hard");
		msgConfig.addDefault("death.anvil", "[player]&7 has been anvil\'d");
		msgConfig.addDefault("death.fire", "[player]&7 forgot he doesn\'t have fire resistance");
		msgConfig.addDefault("death.burning", "[player]&7 was burned alive");
		msgConfig.addDefault("death.elytra", "[player]&7 has died to a elytra malfunction");
		msgConfig.addDefault("death.magma-block", "[player]&7 couldn\'t resist 100°C");
		msgConfig.addDefault("death.lava", "[player]&7 has taken a nose dive into lava");
		msgConfig.addDefault("death.lightning", "[player]&7 climbed a tree during a thunderstorm");
		msgConfig.addDefault("death.potion", "[player]&7 drank the wrong juice");
		msgConfig.addDefault("death.projectile", "[player]&7 has been hit too hard by a moving object");
		msgConfig.addDefault("death.starvation", "[player]&7 became a kid in africa");
		msgConfig.addDefault("death.suffocation", "[player]&7 confused blocks with the void");
		msgConfig.addDefault("death.thorns", "[player]&7 has been nae nae\'d");
		msgConfig.addDefault("death.void", "[player]&7 ran off the wrong bridge");
		
		msgConfig.addDefault("vanish.VanishAlert", "[player]&7 has vanished");
		msgConfig.addDefault("vanish.UnvanishAlert", "[player]&7 has vanished");
		
		msgConfig.options().copyDefaults(true);
		
		// Permissions
		List<String> permissions = new ArrayList<String>();
		permissions.add("bukkit.command.plugins");
		attrConfig.addDefault("permissions.default.id", 0);
		attrConfig.addDefault("permissions.default.nameColor", "white");
		attrConfig.addDefault("permissions.default.chatColor", "gray");
		attrConfig.addDefault("permissions.default.permissions", permissions);
		
		List<String> permissions1 = new ArrayList<String>();
		permissions1.add("minecraft.command.kick");
		attrConfig.addDefault("permissions.trusted.id", 1);
		attrConfig.addDefault("permissions.trusted.nameColor", "blue");
		attrConfig.addDefault("permissions.trusted.chatColor", "white");
		attrConfig.addDefault("permissions.trusted.permissions", permissions1);
		
		List<String> permissions2 = new ArrayList<String>();
		permissions2.add("minecraft.command.difficulty");
		attrConfig.addDefault("permissions.mod.id",2);
		attrConfig.addDefault("permissions.mod.nameColor", "green");
		attrConfig.addDefault("permissions.mod.chatColor", "white");
		attrConfig.addDefault("permissions.mod.permissions", permissions2);
		
		List<String> permissions3 = new ArrayList<String>();
		permissions3.add("Vanish.Alert");
		attrConfig.addDefault("permissions.admin.id", 3);
		attrConfig.addDefault("permissions.admin.nameColor", "red");
		attrConfig.addDefault("permissions.admin.chatColor", "white");
		attrConfig.addDefault("permissions.admin.permissions", permissions3);
		
		List<String> permissions4 = new ArrayList<String>();
		permissions4.add("minecraft.command.deop");
		attrConfig.addDefault("permissions.owner.id", 4);
		attrConfig.addDefault("permissions.owner.nameColor", "gold");
		attrConfig.addDefault("permissions.owner.chatColor", "white");
		attrConfig.addDefault("permissions.owner.permissions", permissions4);
		attrConfig.options().copyDefaults(true);
		
		
		try {
			msgConfig.save(ServerHandler.getMSGFile());
			attrConfig.save(ServerHandler.getAttrFile());
		} catch (IOException e) {
			this.getLogger().severe("Got IOException when saving defaults on the configuration files");
		}
	}
	
	public static FileConfiguration getAttrConfig() {
		return attributesConfig;
	}
	
	public static File getAttrFile() {
		return attributesFile;
	}
	
	public static FileConfiguration getMSGConfig() {
		return messagesConfig;
	}
	
	public static File getMSGFile() {
		return messagesFile;
	}

	public static FileConfiguration getPlayerConfig() {
		return playersConfig;
	}
	
	public static File getPlayerFile() {
		return playersFile;
	}
	
	public static FileConfiguration getBoardsConfig() {
		return scoreboardsConfig;
	}
	
	public static File getBoardsFile() {
		return scoreboardsFile;
	}
	
	public static FileConfiguration getInfoConfig() {
		return infoConfig;
	}
	
	public static File getInfoFile() {
		return infoFile;
	}
	
	public static void reloadConfigs() {
		ServerHandler.attributesConfig = YamlConfiguration.loadConfiguration(attributesFile);
		ServerHandler.messagesConfig = YamlConfiguration.loadConfiguration(messagesFile);
		ServerHandler.playersConfig = YamlConfiguration.loadConfiguration(playersFile);
		ServerHandler.scoreboardsConfig = YamlConfiguration.loadConfiguration(scoreboardsFile);
		ServerHandler.infoConfig = YamlConfiguration.loadConfiguration(infoFile);
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			PermissionHandler.loadPlayerPermissions(p);
		}
	}
	
	public static void saveConfigs() {
		
		try {
			attributesConfig.save(attributesFile);
			messagesConfig.save(messagesFile);
			playersConfig.save(playersFile);
			scoreboardsConfig.save(scoreboardsFile);
			infoConfig.save(infoFile);
		} catch (IOException e) {
			ServerHandler.getPlugin().getLogger().severe("Got IOException when saving files");
		}
	}
}
