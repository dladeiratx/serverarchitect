package ladeira.general;

import org.bukkit.entity.Player;

import ladeira.attributes.PermissionHandler;
import net.md_5.bungee.api.ChatColor;

public class MessageHandler {
	public static String format(String msg, MSGType type) {
		String prefix = "";
		
		switch (type) {
		case info:
			prefix = ChatColor.WHITE + "[" + ChatColor.GRAY + "INFO" + ChatColor.WHITE + "]";
			break;
		case error:
			prefix = ChatColor.WHITE + "[" + ChatColor.RED + "ERROR" + ChatColor.WHITE + "]";
			break;
		case chat:
			prefix = ChatColor.WHITE + "[" + ChatColor.YELLOW + "CHAT" + ChatColor.WHITE + "]";
			break;
		case death:
			prefix = ChatColor.WHITE + "[" + ChatColor.GREEN + "DEATH" + ChatColor.WHITE + "]";
		}
		
		msg = prefix + " " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', msg);
		return msg;
	}
	
	public static String format(String raw_msg, MSGType type, Player p) {
		String msg = "";
		String prefix = "";
		
		switch (type) {
		case info:
			prefix = ChatColor.WHITE + "[" + ChatColor.GRAY + "INFO" + ChatColor.WHITE + "]";
			break;
		case error:
			prefix = ChatColor.WHITE + "[" + ChatColor.RED + "ERROR" + ChatColor.WHITE + "]";
			break;
		case chat:
			prefix = ChatColor.WHITE + "[" + ChatColor.YELLOW + "CHAT" + ChatColor.WHITE + "]";
			break;
		case death:
			prefix = ChatColor.WHITE + "[" + ChatColor.GREEN + "DEATH" + ChatColor.WHITE + "]";
		}
		
		msg = raw_msg.replace("[player]", PermissionHandler.formatName(p.getName(), p.getUniqueId()));
		msg = prefix + " " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', msg);
		return msg;
	}
}
