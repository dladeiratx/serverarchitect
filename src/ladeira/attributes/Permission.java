package ladeira.attributes;

import org.bukkit.ChatColor;

public class Permission {
	private int id;
	private String name;
	private String path;
	private ChatColor color;
	private ChatColor chatColor;
	
	public Permission(int arg0, String arg1, String arg2, ChatColor arg3, ChatColor arg4) {
		id = arg0;
		name = arg1;
		path = arg2;
		color = arg3;
		chatColor = arg4;
	}
	
	public int getID() { return id; }
	public String getName() { return name; }
	public String getPath() { return path; }
	public ChatColor getColor() { return color; }
	public ChatColor getChatColor() { return chatColor; }
}
