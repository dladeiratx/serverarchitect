package ladeira.versions;

import org.bukkit.entity.Player;

import ladeira.general.ServerHandler;

public class v1_15 implements VersionClass {

	@Override
	public void hidePlayer(Player p, Player hide) {
		p.hidePlayer(ServerHandler.getPlugin(), hide);
	}

	@Override
	public void showPlayer(Player p, Player show) {
		p.showPlayer(ServerHandler.getPlugin(), show);
	}

}
