package ladeira.versions;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class VersionHandler {
	
	private double ver; // To store ver number
	private v1_15 v115;
	private v1_12 v112;
	
	public VersionHandler() {
		String bukkitver = Bukkit.getBukkitVersion();
		
		if (bukkitver.contains("1.15")) {
			ver = 1.15;
		} else if (bukkitver.contains("1.14")) {
			ver = 1.14;
		} else if (bukkitver.contains("1.13")) {
			ver = 1.13;
		} else if (bukkitver.contains("1.12")) {
			ver = 1.12;
		}
		v115 = new v1_15();
		v112 = new v1_12();
	}
	
	public double getVersion() { return ver; }
	
	public void hidePlayer(Player hide, Player p) {
		if (ver >= 1.13) {
			v115.hidePlayer(p, hide);
		} else {
			v112.hidePlayer(p, hide);
		}
	}
	
	public void showPlayer(Player show, Player p) {
		if (ver >= 1.13) {
			v115.showPlayer(p, show);
		} else {
			v112.showPlayer(p, show);
		}
	}
	
	public ItemStack getGreenWool() {
		if (ver >= 1.13) {
			return new ItemStack(Material.valueOf("GREEN_WOOL"));
		} else {
			return new ItemStack(Material.valueOf("WOOL"), 1,(byte) 13);
		}
	}
	
	public Material getSkull() {
		if (ver >= 1.13) {
			return Material.valueOf("LEGACY_SKULL_ITEM");
		} else {
			return Material.valueOf("SKULL_ITEM");
		}
	}
}
