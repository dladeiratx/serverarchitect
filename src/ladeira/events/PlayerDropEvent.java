package ladeira.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import ladeira.vanish.VanishInv;

public class PlayerDropEvent implements Listener {
	
	@EventHandler
	public void onPlayerDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		VanishInv vanish = new VanishInv();
		ItemStack item = e.getItemDrop().getItemStack();
		if (vanish.isWool(item)) { // If the item is a compass toggle gamemodes
			vanish.swapGamemodes(p);
			e.setCancelled(true);
		} else if (vanish.isCompass(item)) {
			vanish.openTeleportInventory(p); // Open inventory to pick which player to teleport to
			e.setCancelled(true);
		}
	}
}