package ladeira.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import ladeira.vanish.VanishInv;

public class PlayerUseEvent implements Listener{
	
	@EventHandler
	public void onInteractEvent(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action action = e.getAction();
		ItemStack item = e.getItem();
		VanishInv vanish = new VanishInv();
		if ((action.equals(Action.RIGHT_CLICK_AIR) || (action.equals(Action.RIGHT_CLICK_BLOCK)) || (action.equals(Action.LEFT_CLICK_AIR) || (action.equals(Action.LEFT_CLICK_BLOCK))))) {
			if (vanish.isWool(item)) { // If the item is a compass toggle gamemodes
				vanish.swapGamemodes(p);
				e.setCancelled(true);
			} else if (vanish.isCompass(item)) {
				vanish.openTeleportInventory(p); // Open inventory to pick which player to teleport to
				e.setCancelled(true);
			}
		}
	}
}