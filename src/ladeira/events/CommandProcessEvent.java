package ladeira.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import ladeira.general.ServerHandler;

public class CommandProcessEvent implements Listener { // TODO Remove this class and include it in custom command prefix in ChatEvent

	@EventHandler(priority = EventPriority.LOWEST)
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().toLowerCase().equals("/reload")) {
			ServerHandler.reloadConfigs();
			e.setCancelled(true);
		}
	}
}
