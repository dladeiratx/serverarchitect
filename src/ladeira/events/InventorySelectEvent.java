package ladeira.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import ladeira.general.MSGType;
import ladeira.general.MessageHandler;
import ladeira.vanish.VanishInv;

public class InventorySelectEvent implements Listener {

	@EventHandler
	public void playerClickEvent(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		ItemStack item = e.getCurrentItem();
		VanishInv vInv = new VanishInv();
		
		if (e.getView().getTitle().equals(vInv.getInvName())) {
			System.out.println("item name: " + item.getItemMeta().getDisplayName());
			System.out.println("playername: " + Bukkit.getPlayer(ChatColor.stripColor(item.getItemMeta().getDisplayName())).getName());
			Player target = Bukkit.getPlayer(ChatColor.stripColor(item.getItemMeta().getDisplayName()));
			p.teleport(target.getLocation()); // Teleport player to a player with the name of the item that was clicked
			p.sendMessage(MessageHandler.format("You have teleported to " + target.getName(), MSGType.info));
			e.setCancelled(true);
		} else if (vInv.isWool(item)) {
			vInv.swapGamemodes(p);
			e.setCancelled(true);
		}
	}
}