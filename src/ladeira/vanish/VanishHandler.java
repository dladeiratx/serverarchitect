package ladeira.vanish;

import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ladeira.attributes.PermissionHandler;
import ladeira.general.MSGType;
import ladeira.general.MessageHandler;
import ladeira.general.ServerHandler;
import ladeira.versions.VersionHandler;

public class VanishHandler {
	
	private static HashSet<UUID> vanished = new HashSet<UUID>();
	private static String JoinMSG;
	private static String LeaveMSG;
	private static String VanishAlertMSG;
	private static String UnvanishAlertMSG;
	private static VersionHandler ver;
	private static FileConfiguration info;
	
	public static void loadVanishMessages() {
		FileConfiguration msgConfig = ServerHandler.getMSGConfig();
		info = ServerHandler.getInfoConfig();
		ver = new VersionHandler();
		
		JoinMSG = MessageHandler.format(msgConfig.getString("join"), MSGType.info);
		LeaveMSG = MessageHandler.format(msgConfig.getString("leave"), MSGType.info);
		VanishAlertMSG = msgConfig.getString("vanish.VanishAlert");
		UnvanishAlertMSG = msgConfig.getString("vanish.UnvanishAlert");
	}
	
	public static void listVanishedPlayers(Player p) {
		
		p.sendMessage(ChatColor.WHITE + "----- " + ChatColor.GREEN + "Vanished Players" + ChatColor.WHITE + " -----"); // Nice dash line to start lsit
		if (vanished.size() <= 0) { p.sendMessage(ChatColor.RED + "Nobody is vanished"); } // If nobody is vanished
		
		// Loop through all the vanished players and send their name
		else { for (UUID vanished : vanished) { p.sendMessage(ChatColor.WHITE + "- " + PermissionHandler.formatName(Bukkit.getPlayer(vanished).getName(), vanished)) ; } }
		
		p.sendMessage(ChatColor.WHITE + "----------------------------"); 
	}
	
	public static void vanishPlayer(Player p) {
		saveInventory(p); // Save the player's inventory to retrive later
		
		// For every player in the game hide the player
		for (Player online : Bukkit.getOnlinePlayers()) { ver.hidePlayer(p, online); }
		
		Bukkit.broadcastMessage(LeaveMSG); // Broadcast fake leave message
		
		// Alert all Players with Vanish.Alert Permission or if they are OP
		for (Player online : Bukkit.getOnlinePlayers()) { if (online.isOp() || online.hasPermission("Vanish.Alert")) { MessageHandler.format(VanishAlertMSG, MSGType.info, p); } }
		
		VanishHandler.vanished.add(p.getUniqueId()); // Add player to vanished list
	}
	
	public static void unvanishPlayer(Player p) {
		loadInventory(p); // Retrive the players inventory before player has vanished
		
		// For every player in the game show the player
		for (Player online : Bukkit.getOnlinePlayers()) { ver.showPlayer(p, online); }
		
		Bukkit.broadcastMessage(JoinMSG); // Broadcast fake join message
		
		// Alert all Players with Vanish.Alert Permission
		for (Player online : Bukkit.getOnlinePlayers()) { if (online.hasPermission("Vanish.Alert")) { online.sendMessage(MessageHandler.format(UnvanishAlertMSG, MSGType.info, p)); } }
		
		VanishHandler.vanished.remove(p.getUniqueId()); // Add player to vanished list
	}
	
	private static void saveInventory(Player p) {
		
		for (int i = 0; i < p.getInventory().getContents().length - 5; i++ ) { // Loop over player inventory and save it in config
			info.set("vanish.inventories." + p.getUniqueId() + ".inventory." + i, p.getInventory().getContents()[i]);
		}
		
		for (int i = 0; i < 4; i++ ) { // Loop over player armor and save it in config
			info.set("vanish.inventories." + p.getUniqueId() + ".armor." + i, p.getInventory().getArmorContents()[i]);
		}
		
		ServerHandler.saveConfigs(); // Save the config values
		p.getInventory().clear(); // Clear the player's inventory so a new inventory can be added
		
		VanishInv inv = new VanishInv(); // Get the vanish inventory class
		inv.setInv(p); // Set the player's new inventory
	}
	
	private static void loadInventory(Player p) {
		ItemStack[] inventory = new ItemStack[37]; // TODO Make or check for compatibility with other versions that do/don't have the off hand slot
		ItemStack[] armor = new ItemStack[4];
		
		for (int i = 0; i < 36; i++) {
			inventory[i] = info.getItemStack("vanish.inventories." + p.getUniqueId() + ".inventory." + i);
		}
		
		for (int i = 0; i < 4; i++) {
			armor[i] = info.getItemStack("vanish.inventories." + p.getUniqueId() + ".armor." + i);
		}
		
		p.getInventory().setContents(inventory);
		p.getInventory().setArmorContents(armor);
		p.updateInventory();
	}
	
	public static boolean isVanished(Player p) {
		return vanished.contains(p.getUniqueId());
	}
	
	public static HashSet<UUID> getMap() {
		return vanished;
	}
	
	public void setVanishInv(Player p) {
		
	}
}